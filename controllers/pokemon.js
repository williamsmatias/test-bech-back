const { request, response } = require('express');
const axios = require('axios');

const apiUrl = process.env.API_POKEMON;

const getPokemons = async (req = request, res = response) => {
    try {
        const { limit = 150, offset = 0 } = req.query;
        const url = `${apiUrl}/pokemon?limit=${limit}&offset=${offset}`;
        const response = await axios.get(url);
        const data = response.data.results;
        const pokemons = data.map(pokemon => pokemon.name);
        res.json({
            ok: true,
            pokemons
        })
    } catch (error) {
        console.log('Error: ', error);
        res.status(500).json({
            ok: false,
            msg: 'Error, request failed'
        })
    }
}

const getPokemonByName = async (req = request, res = response) => {
    try {
        const { pokemonName } = req.query;
        const url = `${apiUrl}/pokemon/${pokemonName}`;
        const response = await axios.get(url);
        const abilities = response.data.abilities;
        res.json({
            ok: true,
            abilities
        })
    } catch (error) {
        console.log('Error: ', error);
        res.status(500).json({
            ok: false,
            msg: 'Error, request failed'
        })
    }
}

module.exports = { getPokemons, getPokemonByName };

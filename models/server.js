const express = require('express');

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.pokemonsPath = '/api/pokemons'

        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.app.use(express.json());
    }

    routes() {
        this.app.use(this.pokemonsPath, require('../routes/pokemon'));
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Server init in port ${process.env.PORT}`);
        })
    }
}

module.exports = Server;
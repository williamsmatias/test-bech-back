const { Router } = require('express');
const { check } = require('express-validator');
const { getPokemons, getPokemonByName } = require('../controllers/pokemon'); 
const { validateFields } = require('../middlewares/validate-fields');

// URL: api/pokemons

const router = Router();

router.get('/', getPokemons);

router.get('/name',[
    check('pokemonName', 'The Pokemon name is mandatory').not().isEmpty(),
    validateFields
], getPokemonByName);

module.exports = router;